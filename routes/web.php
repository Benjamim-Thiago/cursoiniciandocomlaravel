<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('cliente/json/{param}', 'Client\ClientController@indexTwo')->name('client.index.json');

Route::get('cliente', 'Client\ClientController@index')->name('client.index');
Route::get('cliente/adicionar', 'Client\ClientController@add')->name('client.add');
Route::post('cliente/adicionar/salvar', 'Client\ClientController@store')->name('client.save');
Route::get('cliente/editar/{id}', 'Client\ClientController@edit')->name('client.edit');
Route::put('cliente/alterar/{id}', 'Client\ClientController@update')->name('client.update');
Route::get('cliente/remover/{id}', 'Client\ClientController@destroy')->name('client.remove');

Route::get('cliente/detalhe/{id}', 'Client\ClientController@detail')->name('client.detail');
Route::get('telefone/adicionar/{id}', 'Phone\PhoneController@add')->name('phone.add');
Route::post('telefone/adicionar/salvar/{id}', 'Phone\PhoneController@store')->name('phone.save');
Route::get('telefone/editar/{id}', 'Phone\PhoneController@edit')->name('phone.edit');
Route::put('telefone/alterar/{id}', 'Phone\PhoneController@update')->name('phone.update');
Route::get('telefone/remover/{id}', 'Phone\PhoneController@destroy')->name('phone.remove');
