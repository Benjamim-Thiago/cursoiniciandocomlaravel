<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PhoneResquest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'title.required'=>'Dígite o Titulo.',
            'title.min'=>'Titulo deve conter no mínimo 2 caracteres.',
            'title.max'=>'Titulo deve conter no maximo 255 caracteres.',
            'phone.required'=>'Dígite o Telefone.'
        ];
    }

    public function rules()
    {
        return [
            'title'=>'required|min:2|max:255',
            'phone'=>'required'
        ];
    }
}
