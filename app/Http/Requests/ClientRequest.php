<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    
    public function messages()
    {
        return[
            'name.required'=>'Dígite o nome.',
            'name.max'=>'É permitido somente 255 caracteres.',
            'name.min'=>'Nome deve conter pelo menos 2 dígitos.',
            'email.required'=>'Dígite o E-mail.',
            'email.email'=>'Dígite um E-mail valido',
            'address.required'=>'Dígite o Endereço',
            'address.max'=>'É permitido somente 255 caracteres',
            'address.min'=>'Endereço deve conter pelo menos 3 dígitos'            
        ];
    }

    public function rules()
    {
        return [
            'name'=>'required|min:2|max:255',
            'email'=>'required|email|max:255',
            'address'=>'required|min:3|max:255'
        ];
    }
}
