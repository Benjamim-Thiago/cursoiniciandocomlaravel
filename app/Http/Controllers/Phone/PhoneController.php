<?php

namespace App\Http\Controllers\Phone;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PhoneResquest as PhoneResquest;


class PhoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }
    
    public function add($id)
    {
        $client = \App\Client::find($id); 
        return view('phone.add', compact('client'));
    }

    public function edit($id)
    {
        $phone = \App\Phone::find($id);
        if(!$phone)
        {
            \Session::flash('flash_message',[
    		    'msg'=>"Telefone não exite.",
    		    'class'=>"alert-danger"
    	    ]);
            return redirect()->route('client.detail',$phone->client->id);
        }      
        return view('phone.edit', compact('phone'));
    }

    public function store(PhoneResquest $request, $id)
    {
        $phone = new \App\Phone;
        $phone->title = $request->input('title');
        $phone->phone = $request->input('phone');

        \App\Client::find($id)->addphones($phone);

        \Session::flash('flash_message',[
    		'msg'=>"Telefone adicionado com Sucesso!",
    		'class'=>"alert-success"
    	]);

    	return redirect()->route('client.detail', $id);  
    }

    public function update(PhoneResquest $request, $id)
    {
        $phone = \App\Phone::find($id);
        $phone->update($request->all());
        \Session::flash('flash_message',[
    	    'msg'=>"Telefone alterado com Sucesso!",
    	    'class'=>"alert-success"
    	]);
        return redirect()->route('client.detail',$phone->client->id);      
    }

    public function destroy($id)
    {
        $phone = \App\Phone::find($id);

        $phone->delete();

        \Session::flash('flash_message',[
            'msg'=>"Telefone deletado com Sucesso!",
            'class'=>"alert-success"
        ]);

        return redirect()->route('client.detail',$phone->client->id);
    }
}
