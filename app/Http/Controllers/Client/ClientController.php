<?php

namespace App\Http\Controllers\Client;

use App\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\ClientRequest as ClientRequest;
use App\Client as Client;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $client = \App\Client::paginate(10);
        return view('client.index', compact('client'));
    }

    public function indexTwo($param)
    {   
        $client = new Client;
        $result = $client->search($param);
        return $result;
    }
    
    public function add()
    {
        return view('client.add');
    }

    public function detail($id)
    {
        $client= \App\Client::find($id);
        return view('client.detail', compact('client'));
    }

    public function create()
    {
        //
    }

    public function store(ClientRequest $request)
    {
        $client = new \App\Client;
        $client->create($request->all());
        if($client){
            \Session::flash('flash_message',[
                'msg'=>"Cliente adicionado com Sucesso!",
                'class'=>"alert-success"
            ]);
        }else{
    	    return redirect()->route('client.add')->withInput($request->all());  
        }
    	return redirect()->route('client.add');  
    }

    public function edit($id)
    {
        $client = \App\Client::find($id);
        if(!$client)
        {
            \Session::flash('flash_message',[
    		    'msg'=>"Cliente não exite, por favor adicione o cliente.",
    		    'class'=>"alert-success"
    	    ]);
            return redirect()->route('client.add');
        }      
        return view('client.edit', compact('client'));
    }

    public function update(ClientRequest $request, $id)
    {
        \App\Client::find($id)->update($request->all());
        \Session::flash('flash_message',[
    	    'msg'=>"Cliente alterado com Sucesso!",
    	    'class'=>"alert-success"
    	]);
        return redirect()->route('client.index');      
    }
    
    public function show($id)
    {
        //
    }

    public function destroy($id)
    {
        $client = \App\Client::find($id);

        if(!$client->removePhones()){
            \Session::flash('flash_message',[
                'msg'=>"Registro não pode ser deletado!",
                'class'=>"alert-danger"
            ]);
            return redirect()->route('client.index');
        }

        $client->delete();

        \Session::flash('flash_message',[
            'msg'=>"Cliente deletado com Sucesso!",
            'class'=>"alert-success"
        ]);

        return redirect()->route('client.index'); 
    }
}
