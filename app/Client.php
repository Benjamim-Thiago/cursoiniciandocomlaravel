<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Client extends Model
{
    protected $fillable = ['name', 'email', 'address'];
    public function phones()
    {
    	return $this->hasMany('App\Phone');
    }

    public function addphones(Phone $phone)
    {
    	return $this->phones()->save($phone);
    }

    public function removePhones()
    {
    	foreach ($this->phones as $phone)
        {
    		$phone->delete();
    	}

    	return true;
    }

    public function search($param)
    {
       $select = DB::table('clients')
          ->where('name','like','%'.$param.'%')
          ->get();
          return $select;
    }
}
