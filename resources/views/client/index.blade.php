@extends('layouts.app')

@section('content')

<div class="container">
    <input onkeypress="search()" id="search" type="" name="" value="">
    <div hidden class="resultado" style="height: 50px; background: #FFF; width: 250px"></div>
</div>


<script>
    function search(){
        var search = $("#search");
        console.log(search.val());
        if(search.val().length >= 1){
            $.get("/cliente/json/"+search.val(), function(response){
                if(response.length > 0){
                    $('.resultado').show('fast');
                    $.each(response, function(index,res){
                        $('.resultado').html(res.name);
                        console.log(res.name);
                    })
                }
            });
        }
    }
</script>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
               <ol class="breadcrumb panel-heading">
                    <li class="active">Clientes</li>
                </ol>
                <div class="panel-body">
                    <p>
                        <a class="btn btn-info" href="{{ route('client.add') }}">Adicionar</a>
                    </p>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <th>#</th>
                                <th>Nome</th>
                                <th>E-mail</th>
                                <th>Endereço</th>
                                <th>Ação</th>
                            </thead>
                            <tbody>
                                @foreach($client as $cli)
                                <tr>    
                                    <th>{{ $cli->id }}</th>
                                    <th><a href="{{ route('client.detail', $cli->id) }}">{{ $cli->name }}</a></th>
                                    <th>{{ $cli->email }}</th>
                                    <th>{{ $cli->address }}</th>
                                    <th>
                                        <a class="btn btn-warning" href="{{ route('client.edit', $cli->id) }}">Editar</a>
                                        <a class="btn btn-danger" href="javascript:(confirm('Deletar esse registro?') ? window.location.href='{{route('client.remove',$cli->id)}}' : false)">Excluir</a>
                                    </th>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>    
                    <div align="center">
                        {!! $client->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
