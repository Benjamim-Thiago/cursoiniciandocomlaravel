@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
               <ol class="breadcrumb panel-heading">
                    <li><a href="{{ route('client.index') }}">Clientes</a></li>
                    <li class="active">Detalhes de cliente</li>
                </ol>
                <div class="panel-body">
                    <p><b>Cliente:</b> {{ $client->name }}</p>
                    <p><b>E-mail:</b> {{ $client->email }}</p>
                    <p><b>Endereço:</b> {{ $client->address }}</p>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <th>#</th>
                                <th>Titulo</th>
                                <th>Numero</th>
                                <th>Ação</th>
                            </thead>
                            <tbody>
                                @foreach($client->phones as $phone)
                                <tr>    
                                    <th>{{ $phone->id }}</th>
                                    <th>{{ $phone->title }}</th>
                                    <th>{{ $phone->phone }}</th>
                                    <th>
                                        <a class="btn btn-warning" href="#">Editar</a>
                                        <a class="btn btn-danger" href="javascript:(confirm('Deletar esse registro?') ? window.location.href='#' : false)">Excluir</a>
                                    </th>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <p>
                            <a class="btn btn-info" href="{{ route('phone.add', $client->id) }}">Adicionar Telefone</a>
                        </p>
                    </div>    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
