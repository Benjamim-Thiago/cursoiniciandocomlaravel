@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <ol class="breadcrumb panel-heading">
                    <li><a href="{{ route('client.index') }}">Clientes</a></li>
                    <li><a href="{{ route('client.detail', $phone->client->id) }}">Detalhe</a></li>
                    <li class="active">Editar</li>
                </ol>
                <div class="panel-body">                                    
                    <form action="{{ route('phone.update', $phone->id) }}" method="post">
                        {{ csrf_field() }}

                        <input type="hidden" name="_method" value="put">
                        <div class="form-group">
                            <h4><b>Cliente:</b> {{ $phone->client->name }}</h4>
                        </div>

                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <label for="title">Titulo</label>
                            <input type="text" name="title" class="form-control" placeholder="Titulo do Telefone" value="{{ $phone->title }}">
                            @if($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                            <label for="phone">Telefone</label>
                            <input type="text" name="phone" class="form-control" placeholder="Telefone"  value="{{ $phone->phone }}">
                            @if($errors->has('phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>
                        <button class="btn btn-primary">Editar</button>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
